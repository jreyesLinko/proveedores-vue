export default {
  SET_LANG (state, locale) {
    if (state.lang.locales.indexOf(locale) !== -1) {
      state.lang.locale = locale
    }
  }
}
